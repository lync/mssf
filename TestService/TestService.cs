﻿using TestService.Interfaces;
using Microsoft.ServiceFabric.Actors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace TestService
{
    /// <remarks>
    /// Each ActorID maps to an instance of this class.
    /// The ITestService interface (in a separate DLL that client code can
    /// reference) defines the operations exposed by TestService objects.
    /// </remarks>
    internal class TestService : StatelessActor, ITestService
    {
        Task<string> ITestService.DoWorkAsync()
        {
            // TODO: Replace the following with your own logic.
            ActorEventSource.Current.ActorMessage(this, "Doing Work");

            return Task.FromResult("Work result");
        }
    }
}
