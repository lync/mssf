﻿using SampleService.Interfaces;
using Microsoft.ServiceFabric.Actors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SampleService
{
    /// <remarks>
    /// Each ActorID maps to an instance of this class.
    /// The ISampleService interface (in a separate DLL that client code can
    /// reference) defines the operations exposed by SampleService objects.
    /// </remarks>
    internal class SampleService : StatelessActor, ISampleService
    {
        Task<string> ISampleService.DoWorkAsync()
        {
            // TODO: Replace the following with your own logic.
            ActorEventSource.Current.ActorMessage(this, "Doing Work");

            return Task.FromResult("Work result");
        }
    }
}
